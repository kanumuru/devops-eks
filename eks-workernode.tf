resource "aws_eks_node_group" "berkeley-eks-wn" {
  cluster_name    = aws_eks_cluster.berkeley-eks.name
  node_group_name = "worker-group-1"
  node_role_arn   = aws_iam_role.berkeley-eks-wn-role.arn
  subnet_ids      = [aws_subnet.public-subnet1.id, aws_subnet.public-subnet2.id]
  instance_types  = ["t3.small"]
  remote_access {
    ec2_ssh_key = aws_key_pair.generated_key.key_name
    source_security_group_ids = [aws_security_group.berkeley-eks-sg.id]
  }
  scaling_config {
    desired_size = 1
    max_size     = 1
    min_size     = 1
  }

  update_config {
    max_unavailable = 1
  }
  tags = {
    Name = "ekswokerapp"
  }
  # Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
  # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
  depends_on = [
    aws_iam_role_policy_attachment.berkeley-wn-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.berkeley-wn-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.berkeley-wn-AmazonEC2ContainerRegistryReadOnly,
  ]
}

resource "aws_eks_node_group" "berkeley-eks-wn2" {
  cluster_name    = aws_eks_cluster.berkeley-eks.name
  node_group_name = "worker-group-2"
  node_role_arn   = aws_iam_role.berkeley-eks-wn-role.arn
  subnet_ids      = [aws_subnet.public-subnet1.id, aws_subnet.public-subnet2.id]
  instance_types  = ["t3.small"]
  remote_access {
    ec2_ssh_key = aws_key_pair.generated_key.key_name
    source_security_group_ids = [aws_security_group.berkeley-eks-sg.id]
  }
  
  scaling_config {
    desired_size = 1
    max_size     = 1
    min_size     = 1
  }


  update_config {
    max_unavailable = 1
  }
  tags = {
    Name = "ekswokerapp"
  }
  # Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
  # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
  depends_on = [
    aws_iam_role_policy_attachment.berkeley-wn-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.berkeley-wn-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.berkeley-wn-AmazonEC2ContainerRegistryReadOnly,
  ]
}

resource "aws_security_group" "berkeley-eks-sg" {
  name        = "berkeley-eks-sg"
  description = "Allow all inbound traffic"
  vpc_id      = aws_vpc.berkeley-eks-vpc.id
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 30080
    to_port     = 30080
    protocol    = "tcp"
  }
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
  }
  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
  }
  tags = {
    Name = "berkeley-eks-sg"
  }
}

#Generating Key Pair
resource "tls_private_key" "example" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "generated_key" {
  key_name   = "berkeley-eks-key"
  public_key = tls_private_key.example.public_key_openssh
}
