resource "aws_vpc" "berkeley-eks-vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "berkeley-eks-${random_string.suffix.result}"
  }
}
